# WShop
Das ist das Repository für die WShop Applikation

## Installationsanleitung
Die Installationsanleitung kann [hier](documentation/installation-manual.md) gefunden werden.

## Weitere Dokumente

Folgende Dokumente können im Order **/documentation** gefunden werden:
* Technische Dokumentation (PDF)
* Test Protokoll (PDF)
* Datenbankexport Anleitung (MD)