# How to create full backup of WShop database (including user)

## 1. Create backup of database

### Syntax:

```
mysqldump -u [username] -p --default-character-set=utf8 --databases [db-name] > [filepath]
```

### Example:

```
mysqldump -u root -p --default-character-set=utf8 --databases wshop_db > C:\xampp\htdocs\wshop\db\db-export.sql
```

## 2. Add user backup to file
Add following code at the bottom of the sql script
```
CREATE USER 'wshopdb-admin'@'localhost' IDENTIFIED BY 'UgVNvpx2S2CHnsCU';
GRANT SELECT, INSERT, DELETE, EXECUTE, SHOW VIEW, TRIGGER, UPDATE on wshop_db.* TO 'wshopdb-admin'@'localhost';
FLUSH PRIVILEGES;
```

# How to import database backup

## Syntax:


```
mysql -u [username] -p  < [filepath]
```

## Example

```
mysql -u root -p < C:\xampp\htdocs\wshop\db\db-export.sql
```
