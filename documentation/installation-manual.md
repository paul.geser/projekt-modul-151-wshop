# Installationsanleitung

## Voraussetzungen
* [XAMPP](https://www.apachefriends.org/download.html) oder [MAMP](https://www.mamp.info/en/downloads/) ist installiert

## 1. Alle notwendigen Dateien auf lokale Umgebung laden
Die notwendingen Dateien können über zwei Wege heruntergeladen werden:
* 1. Git Clone
* 2. Zip Download
### 1.1 Git Clone
Führen Sie folgenden Command in ihrer CMD/Powershell/GIT Bash/Terminal aus:
```
git clone git@gitlab.com:paul.geser/projekt-modul-151-wshop.git
```
Nun wird das WShop Git Repository auf der lokalen Umgebung geclont und somit alle notwendigen Dateien heruntergeladen.

### 1.2 Zip Downlaod
Navigieren Sie zu folgender URL: https://gitlab.com/paul.geser/projekt-modul-151-wshop

Klicken Sie auf folgenden Button:

![Zip Download Picture](assets/download-picture.jpg)

Nun wird das WShop Projekt mit allen notwendigen Dateien in Form eines Zips auf der lokalen Umgebung heruntergeladen.

Nach dem das Zip erfolgreich heruntergeladen wurde, muss dieses noch extrahiert werden.


## 2. WShop Projekt Order ins Root verschieben
Als nächster Schritt muss der WShop Projekt Ordner ins HTDOCS Root verschoben werden. Verschieben Sie den zuvor heruntergeladenen WShop Projekt Ordner in das HTDOCS Root Verzeichnis. Die Default HTDOCS Pfäde sind folgend aufzufinden:

### Für XAMPP:

#### Windows:
`C:\xampp\htdocs`
#### MAC:
`/Applications/XAMPP/htdocs`
### Für MAMP

#### Windows:
`C:\MAMP\htdocs`
#### MAC:
`/Applications/MAMP/htdocs`

## 3. Datenbank importieren (inkl. Benutzer)
Als letzter Schritt muss die Datenbank(inklusive Benutzer) auf dem lokalen MySQL Server importiert werden. Hierfür wurde ein Datenbankexport mit Benutzererstellung kreiert. Diese Datei ist unter folgendem Pfad zu finden:
#### Windows
```
[WShop_Project_Root_Path]\db\db-export.sql
```
#### Mac
```
[WShop_Project_Root_Path]/db/db-export.sql
```
Als ersten Schritt starten Sie Ihren MySQL Datenbank Server.

Öffnen Sie als zweiten Schritt die Shell von entweder XAMPP oder MAMP.

Führen Sie folgenden Commands in der Shell aus um die Datenbank + Benutzer zu importieren:
#### Windows
```
mysql -u root -p < [WShop_Project_Root_Path]\db\db-export.sql
```

#### Mac
```
mysql -u root -p < [WShop_Project_Root_Path]/db/db-export.sql
```

Nachdem der obengenannte MySQL Command erfolgreich ausgeführt wurde, wurde die Datenbank und der Benutzer vollständing importiert.

## 4. Los gehts...
Starten Sie ihren Apache Server. Danach navigieren Sie zu http://localhost/wshop/
