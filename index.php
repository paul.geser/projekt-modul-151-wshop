<?php

// Alle Fehlermeldungen anzeigen
ini_set('display_errors', 1);
error_reporting(E_ALL);

include('shared/mysql/db_connector.inc.php');
include('shared/helper/db_helper.php');
include('shared/helper/decimal.helper.php');
include('shared/helper/attachment_helper.php');

session_start();

ob_start();

?>

<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <style>
        <?php include './css/all.css'; ?>
    </style>
    <link rel='shortcut icon' type='image/x-icon' href='assets/favicon-32x32.ico' />
    <title>WShop</title>
</head>

<body>
    <div>
        <?php
        $error = '';
        include('layout/topbar.php');
        ?>
        <div class="index-component-content-main-box">
            <?php
            include('shared/controller/controller.php');
            ?>
        </div>
        <?php
        include('layout/footer.php');
        ?>
    </div>
</body>

</html>