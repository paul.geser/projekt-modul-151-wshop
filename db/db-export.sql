-- MariaDB dump 10.19  Distrib 10.4.20-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: wshop_db
-- ------------------------------------------------------
-- Server version	10.4.20-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `wshop_db`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `wshop_db` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `wshop_db`;

--
-- Table structure for table `attachment`
--

DROP TABLE IF EXISTS `attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(45) NOT NULL,
  `old_filename` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attachment`
--

LOCK TABLES `attachment` WRITE;
/*!40000 ALTER TABLE `attachment` DISABLE KEYS */;
INSERT INTO `attachment` VALUES (25,'616dead5b300f-1634593493.png','test1.png'),(26,'616deb08f26c6-1634593544.png','test2.png'),(27,'616deb44e0a43-1634593604.png','test3.png'),(28,'616deb6989bd1-1634593641.png','test4.png'),(29,'616dec04ec711-1634593796.jpg','MacBook_Air_Space_Gray_PDP_Image_Position-1_M1_Chip__USEN.jpg'),(30,'616dec7505698-1634593909.jpg','Google-Pixel-4A-128GB-Just-Black-0810029930031-06112020-01-p.jpg'),(31,'616decdb37e3d-1634594011.jpg','817DLnXqEYL._AC_SY450_.jpg'),(32,'616ded458ef6f-1634594117.jpg','iphone-13-pro-max-graphite-select.jpg'),(33,'616dedb62cc5a-1634594230.jpg','56366_makasar 1.jpg'),(34,'616dede72392f-1634594279.jpg','image.jpg'),(35,'616deea39dfa5-1634594467.jpg','7574.02_l_1.jpg'),(41,'61771035a25de-1635192885.jpg','P047770.jpg'),(42,'6177106b80838-1635192939.jpg','IMG_2944.jpg'),(43,'617710ba65abc-1635193018.jpg','Neues Projekt.jpg'),(44,'61771124c0a9d-1635193124.jpg','koda-16_product-shoot_fullres-4-1500px_1024x1024.jpg');
/*!40000 ALTER TABLE `attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `short_description` varchar(500) NOT NULL,
  `long_description` text DEFAULT NULL,
  `price` float NOT NULL,
  `creation_date` date NOT NULL,
  `owner` int(11) NOT NULL,
  `bought` tinyint(4) DEFAULT 0,
  `new_owner` int(11) DEFAULT NULL,
  `picture_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Product_User_idx` (`owner`),
  KEY `fk_Product_User1_idx` (`new_owner`),
  KEY `fk_Product_Attachment1_idx` (`picture_id`),
  CONSTRAINT `fk_Product_Attachment1` FOREIGN KEY (`picture_id`) REFERENCES `attachment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Product_User` FOREIGN KEY (`owner`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Product_User1` FOREIGN KEY (`new_owner`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (3,'Giardimo Makasar','Hergestellt aus ausgesuchtem, ge&ouml;ltem Akazienholz 100% FSC-zertifiziert aus nachhaltiger Forstwirtschaft, mit Sitz- und R&uuml;ckenkissen aus hochwertigem Polyester mit Schaum-/Vliesf&uuml;llung und Reissverschluss, mit ausziehbarem Tablett und zwei Laufrollen, beide Seitenteile einzeln zur Liege abklappbar.','',150,'2021-10-18',10,0,NULL,33),(5,'Google Pixel 4a','Mach tolle Fotos, ohne ein Verm&ouml;gen daf&uuml;r zu zahlen. Funktionen wie &quot;Live HDR+&quot; und der Nachtsichtmodus der Kamera von Pixel 4a machen es m&ouml;glich.','Der intelligente Akku lernt, welche Apps du besonders h&auml;ufig verwendest, und schr&auml;nkt die Akkunutzung f&uuml;r weniger wichtige Apps ein.\r\n\r\nMit Google Assistant kannst du zwischen Apps wechseln und SMS senden, auch wenn du gerade keine Hand frei hast.\r\n\r\nMit Sicherheit smarter: Mit dem Titan M-Chip und Updates f&uuml;r 3 Jahre.\r\n\r\nDie Qualit&auml;t der Kamera ist so &uuml;berragend, dass sie nach den Sternen greift - egal ob du gestochen scharfe Bilder von der Milchstrasse oder Schnappsch&uuml;sse in Profiqualit&auml;t von deinen Liebsten.\r\n\r\nDank HDR+ werden Farbe und Licht automatisch korrigiert, damit deine Aufnahmen noch sch&ouml;ner werden.\r\n\r\nFokus auf das Wesentliche: Im Portr&auml;tmodus kannst du den Hintergrund deiner Fotos k&uuml;nstlerisch weichzeichnen.\r\n\r\nAuch der Akku ist extrem stark und dazu noch ziemlich schlau. Er gibt deinen Lieblings-Apps extra Power und spart sie daf&uuml;r bei weniger genutzten Funktionen ein. So kommst du auch ohne Steckdose entspannt durch den Tag.\r\n\r\nDer Akku l&auml;dt superschnell4 und bringt dich verl&auml;sslich &uuml;ber den Tag.\r\n\r\nMit dem neuen Google Assistant kannst du dein Smartphone bedienen, zwischen Apps wechseln und SMS senden, auch wenn du gerade keine Hand frei hast.',365,'2021-10-18',12,0,NULL,30),(8,'Basetech Schwerlastregal','Das Schwerlastregal ist Einfaches Stecksystem leichte Montage. Metallregal perfekt geeignet f&uuml;r Feuchtr&auml;ume, Keller oder Garage, Pflegeleichtes einbrennlackiertes Metall Mittelsteg zum Erh&ouml;hen der Belastbarkeit.','',15.75,'2021-10-18',10,0,NULL,34),(10,'Corsair K70 TKL','Die mechanische CORSAIR K70 RGB TKL Gaming-Tastatur macht Sie zum Sieger mit einem kompakten Tenkeyless_x0002_Formfaktor, Performance auf Pro-Level dank CORSAIR AXON Hyper-Processing-Technologie und den weltbekannten CHERRY MX-Tastenschaltern.','Gewinnen Sie stilvoll mit einem stabilen Aluminiumrahmen und dynamischer RGB_x0002_Hintergrundbeleuchtung einzelner Tasten. Mithilfe des Turnierschalters werden Makros deaktiviert und Sie wechseln zu ablenkungsfreier Beleuchtung. PBT-Double-Shot-Tastenkappen mit einer St&auml;rke von 1,5 mm verhindern &uuml;ber Jahre hinweg ein Abnutzen, Verblassen und unerw&uuml;nschtes Gl&auml;nzen. Die leistungsstarke iCUE-Software erm&ouml;glicht auf PC und macOS die Programmierung benutzerdefinierter Makros und Tastenneubelegungen. Gleichzeitig bieten dedizierte Medientasten und ein Lautst&auml;rkerad aus Aluminium eine komfortable Mediensteuerung. Die K70 RGB TKL ist mobil und leistungsstark dank eines abnehmbaren USB-Typ-C-Kabels und 8 MB Onboard-Speicher f&uuml;r bis zu 50 Profile, sodass Sie Ihre Profi-Performance &uuml;berallhin mitnehmen k&ouml;nnen.',85.5,'2021-10-18',11,0,NULL,31),(11,'Trisa Raclettino','Romantischer Raclette-Plausch f&uuml;r 2 Personen.','Romantischer Raclette-Plausch f&uuml;r 2 Personen. Feines Raclette und einfaches Grillieren gleichzeitig. Abnehmbare Steinplatte mit grosser W&auml;rmespeicherung und Saftrinne. 2 antihaftbeschichtete Raclette-Pf&auml;nnchen aus robustem Stahl. Verf&uuml;gt &uuml;ber einen EIN / AUS Schalter. Zerlegbar f&uuml;r einfache Reinigung. Rutschfeste Standf&uuml;sse.',14.9,'2021-10-19',11,0,NULL,35),(12,'Apple iPhone 13 Pro','iPhone 13 Pro. Das gr&ouml;sste Upgrade aller Zeiten f&uuml;r das Pro Kamera-System mit drei neuen Kameras f&uuml;r unglaubliche Fotos und Videos bei wenig Licht, Makrofotografie und ProRes Video.','Mach grosse Filme mit dem Kinomodus. iPhone 13 Pro. 6,1&quot; Super Retina XDR Display mit ProMotion. Adaptive 120 Hz Bildwiederholrate, damit sich alles schneller und fl&uuml;ssiger anf&uuml;hlt. Mehr Helligkeit im Freien f&uuml;r Inhalte, die bei Sonnenlicht noch brillanter aussehen. Der ultraschnelle A15 Bionic Chip mit 5-Core GPU sorgt f&uuml;r eine unglaubliche Grafikleistung. Mach mehr mit einem grossen Sprung bei der Batterielaufzeit. Robustes Design mit Ceramic Shield. Edelstahl in chirurgischer Qualit&auml;t. Branchenf&uuml;hrender IP68 Wasserschutz. 5G erm&ouml;glicht superschnelle Downloads, Streaming in hoher Qualit&auml;t und sogar anspruchsvolle Multiplayer-Spiele. Das iPhone 13 Pro hat mehr 5G B&auml;nder f&uuml;r 5G Speed an mehr Orten.',980.9,'2021-10-18',11,0,NULL,32),(13,'Apple MacBook Air &ndash; Late 2020','D&uuml;nnstes, leichtestes Notebook - komplett verwandelt durch den Apple M1 Chip. Bis zu 3,5x mehr CPU Leistung. Bis zu 5x h&ouml;here GPU Geschwin&shy;digkeiten. Fortschritt&shy;&shy;lichste Neural Engine f&uuml;r bis zu 9x schnelleres maschinelles Lernen. Die l&auml;ngste Batterie&shy;laufzeit, die es je bei einem MacBook Air gab. Und ein leises, l&uuml;fterloses Design. Es war noch nie so leicht, so viel Power &uuml;berall mit hinzunehmen.','Er ist da. Erster Chip, der speziell f&uuml;r den Mac entwickelt wurde. Das Apple M1 System auf einem Chip (SoC) ist vollgepackt mit unglaublichen 16 Milliarden Transistoren und vereint CPU, GPU, Neural Engine, I/O und noch viel mehr auf einem winzigen Chip. Mit unglaublicher Performance, speziellen Technologien und branchen&shy;f&uuml;hrender Energie&shy;effizienz ist der M1 nicht einfach der n&auml;chste Schritt f&uuml;r den Mac.\r\n\r\nMit maschi&shy;nellem Lernen (ML) k&ouml;nnen Apps auf dem MacBook Air Fotos automatisch wie ein Profi bearbeiten, eine pr&auml;zisere automatische Erkennung f&uuml;r intelligente Tools wie Zauberst&auml;be und Audiofilter erm&ouml;glichen und noch viel mehr. Das ist nicht nur Rechen&shy;leistung - es ist die Leistung der gesamten Palette von ML Technologien.\r\n\r\nBilder sind auf dem brillanten 13,3&quot; Retina Display mit 2560 x 1600 Aufl&ouml;sung detaillierter und realistischer als je zuvor. Text ist gestochen scharf und klar. Farben sind noch lebendiger. Und das Glas reicht bis an den Rand des Geh&auml;uses, damit dich nichts von diesem Anblick ablenkt.\r\n\r\nDer Bildsignal&shy;prozessor im M1 Chip hilft dir, bei jedem FaceTime Anruf und jeder Videokonferenz gut auszusehen. Drei integrierte Mikrofone sorgen daf&uuml;r, dass du geh&ouml;rt wirst, wenn du telefonierst, eine Notiz diktierst oder Siri nach dem Wetter fragst.\r\n\r\nWenn du mit einer Tastatur nicht nur fantastisch tippen kannst, sondern noch viel mehr, dann ist das fast schon magisch. Mit vorprogrammierten Kurzbefehlen kannst du Dinge noch schneller erledigen. Bitte Siri um Hilfe, &auml;ndere die Tastatursprache, antworte mit dem perfekten Emoji oder finde Dokumente mit Spotlight - es gibt so viel, das du mit nur einem Fingertipp machen kannst. Und die Tasten mit Hintergrund-Beleuchtung und einem Umgebungs&shy;licht&shy;sensor erleichtern das Tippen bei wenig Licht.',1300,'2021-10-18',13,0,NULL,29),(29,'Electrolux Chill Flex Pro Gold','Eintreten, Einatmen, Ausruhen','Traumhafter Schlaf mit ChillFlex Pro Gold. Die optimale Temperatur f&uuml;r Ruhe und\r\nEntspannung. K&uuml;hl und frisch, wenn es draussen heiss ist. Optimales Raumklima bei\r\njeder Witterungsbedingung mit minimalem Energieverbrauch.',350,'2021-10-25',10,0,NULL,41),(30,'CHIC Smart-S Lighting Wheels','Das CHIC-SMART Hoverboard mit LED-R&auml;dern &uuml;berzeugt durch sein einfaches Handling und dem vielseitigen Anwendungsbereich.','Dank den hochwertigen Materialien ist das Hoverboard sehr stabil und &auml;usserst sicher auf den Strassen unterwegs. Die darin verbauten Sensoren dienen der intelligenten Steuerung und bieten dir dadurch eine hohe Stabilit&auml;t beim Fahren. Die flexible Achse erm&ouml;glicht 360&deg;-Drehungen mit dem eigenen K&ouml;rpergewicht. Das heisst, du kannst dich aus dem Stand um deine eigene Achse drehen und meisterst somit auch sehr enge Kurven. Ein besonderes Highlight sind die eingebauten LED-Lichter in beiden R&auml;dern, was das Fahren mit dem Chic Hoverboard zus&auml;tzlich zu einem grossen Spass macht. Das CHIC-SMART Hoverboard mit LED-R&auml;dern im Kurz&uuml;berblick: . Gyroskop- und Beschleunigungssensoren f&uuml;r optimale Balance . Servo-Steuersystem zum genauen Antrieb der Motoren . Zus&auml;tzlicher Fahrspass dank der eingebauten LED-Lichter in beiden R&auml;dern . Nur 2-3 Stunden Ladezeit f&uuml;r eine vollst&auml;ndige Ladung . 10 Km/h Maximalgeschwindigkeit . Akku f&uuml;r bis zu 20 Kilometer . Zwei starke Motoren mit jeweils 300 Watt.',85,'2021-10-25',10,0,NULL,42),(31,'K&auml;rcher K Mini inkl. Schlauchset 10m','Kompakt und leicht: Der K Mini ist K&auml;rchers kleinster Hochdruckreiniger. Durch seine kompakte Gr&ouml;sse und das geringe Gewicht ist er leicht zu transportieren und zu verstauen.','Das kleine Kraftpaket eignet sich ideal f&uuml;r die schnelle und effiziente Reinigung von Balkonfl&auml;chen, Garten- und Terrassenm&ouml;beln sowie Fahrr&auml;dern und Kleinwagen. Die Handhabung ist dabei selbsterkl&auml;rend: Mit nur wenigen Handgriffen k&ouml;nnen die Pistole, das Verl&auml;ngerungsrohr und das Vario Power-Strahlrohr montiert werden. Der Hochdruckschlauch l&auml;sst sich dank Quick Connect-System schnell und einfach in das Ger&auml;t und die Pistole ein- und ausklicken. Der 5 Meter lange und extra d&uuml;nne PremiumFlex-Hochdruckschlauch verhindert l&auml;stige Verknotungen und bietet maximale Flexibilit&auml;t und Bewegungsfreiheit w&auml;hrend des Reinigungsvorgangs. Ein abnehmbarer Zubeh&ouml;rbeh&auml;lter erm&ouml;glicht das ordentliche Verstauen aller mitgelieferten Teile. Nach dem Reinigungsvorgang kann das Stromkabel um den Standfuss des Ger&auml;ts gewickelt und mit einer Klammer fixiert werden. Aufgrund seiner geringen Gr&ouml;sse l&auml;sst sich der K Mini platzsparend im Innen- und Aussenbereich verstauen.\r\n\r\nIm Produktset ist ebenfalls das K&auml;rcher Schlauchset 10m enthalten. Das Schlauchset eignet sich sowohl als Zulaufschlauch f&uuml;r einen Hochdruckreiniger als auch zur Gartenbew&auml;sserung. Das Set besteht aus einem 10-Meter-Schlauch (1/2&quot;), einem G3/4-Hahnanschluss, einer Schlauchkupplung sowie einer Schlauchkupplung mit Aqua Stop. Die Gartenschl&auml;uche der K&auml;rcher Bew&auml;sserungslinie pr&auml;sentieren sich extrem flexibel, robust und knickfest.',125,'2021-10-25',11,0,NULL,43),(32,'Ooni Koda 16','Mit Ooni Koda 16, dem gasbetriebenen Pizzaofen im Freien, der zum Backen von epischen 16-Zoll-Pizzen und k&ouml;stlichen flammengekochten Steaks, Fisch und Gem&uuml;se entwickelt wurde, k&ouml;nnen Sie Ihr Outdoor-Kocherlebnis geniessen.','Das Ooni Koda 16 verbindet ein atemberaubendes Design mit dem Komfort des Kochens mit Gas und verf&uuml;gt &uuml;ber eine breite &Ouml;ffnung, ein grosses Backbrett aus Cordieritstein und eine innovative L-f&ouml;rmige Flamme. Schalten Sie die sofortige Gasz&uuml;ndung ein und Sie sind in 20 Minuten fertig zum Kochen. Wie alle Ooni-Pizza&ouml;fen erreicht Ooni Koda 16 eine Temperatur von bis zu 500 &deg; C - die gl&uuml;hende Hitze, die zum Kochen von perfekt gebackener Pizza aus Stein in 60 Sekunden ben&ouml;tigt wird. Kocht epische 16-Zoll-Pizza mit einer authentischen Basis aus Stein. Robustes, keramikfaserisoliertes, pulverbeschichtetes Stahlgeh&auml;use f&uuml;r &uuml;berlegene W&auml;rmespeicherung und Witterungsbest&auml;ndigkeit. L&auml;uft mit Propangas und ist in nur 20 Minuten nach dem Anz&uuml;nden kochfertig. Kochfl&auml;che: 40,6 cm 1,5 cm Cordierit-Stein-Backbrett L-f&ouml;rmiger Gasbrenner mit Wasserfall-Effektflamme Eingebaute Gasz&uuml;ndung f&uuml;r schnelles und einfaches Garen Gasanschluss zum Anbringen des Schlauches und der Gasquelle Klappbeine f&uuml;r einfachen Transport und Lagerung Robuste, isolierte, pulverbeschichtete Kohlenstoffstahlschale. Inkl. schweizer Gasanschluss (CH-Version).',325.5,'2021-10-25',10,0,NULL,44);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `prename` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(60) DEFAULT NULL,
  `picture_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_User_Attachment1_idx` (`picture_id`),
  CONSTRAINT `fk_User_Attachment1` FOREIGN KEY (`picture_id`) REFERENCES `attachment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (10,'BehindYou','*','*','$2y$10$5CCY81dNCTzltzlEITki6uaAuxZf9/DGBezEgnQsluOp/RAXNuryy','',25),(11,'HowYouDoing','*','*','$2y$10$H6yOjnC6mGbkPhDUUW4f8udLaHHLbUFqIWI6VgVjZUOM2VCqZ2NFC','',26),(12,'HtmlIsAProgramingLanguage','*','*','$2y$10$CFKXU1YaT2FA0WzucVVRBOIAD2pKEit9.kg2y.4NHYl7gUFlomqca','',27),(13,'TestUser','Test','Test','$2y$10$W3VTJyjqej8gmVPOY2nNy.bvW6qnnojdlqUUhioY/P2UJjEQfQjfa','',28);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-25 22:31:44

CREATE USER 'wshopdb-admin'@'localhost' IDENTIFIED BY 'UgVNvpx2S2CHnsCU';
GRANT SELECT, INSERT, DELETE, EXECUTE, SHOW VIEW, TRIGGER, UPDATE on wshop_db.* TO 'wshopdb-admin'@'localhost';
FLUSH PRIVILEGES;
