<?php
$error = $filename = '';
if (isset($_SESSION['id'])) {
    $id = $_SESSION['id'];
    $query = 'SELECT u.*, a.filename, a.old_filename
            FROM user u LEFT JOIN attachment a
            ON u.picture_id = a.id
            where u.id=?';
    $stmt = $mysqli->prepare($query);

    if ($stmt === false) {
        $error .= 'prepare() failed ' . $mysqli->error . '<br />';
    }
    if (!$stmt->bind_param('i', $id)) {
        $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
    }
    if (!$stmt->execute()) {
        $error .= 'execute() failed ' . $mysqli->error . '<br />';
    }

    $result = $stmt->get_result();
    $stmt->close();

    $result = $result->fetch_assoc();

    if (isset($result['filename'])) {
        $filename = $result['filename'];
    }
}
?>
<nav class="navbar is-fixed-top" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">
        <a class="navbar-item" href="index.php">
            <img src="assets/wshop-logo.jpg" width="100">
        </a>
        <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
        </a>
    </div>

    <div id="navbarBasicExample" class="navbar-menu">
        <div class="navbar-start">
            <a class="navbar-item" href="?page=list">
                Alle Produkte
            </a>
            <?php if (isset($_SESSION['loggedin']) and $_SESSION['loggedin']) : ?>
                <a class="navbar-item" href="?page=list&my-items=true">
                    Meine Produkte
                </a>
                <a class="navbar-item" href="?page=edit-item&mode=create">
                    Neues Produkt erstellen
                </a>
            <?php endif ?>
        </div>

        <div class="navbar-end">

            <div class="navbar-item">
                <form method="get">
                    <input type="hidden" name="page" value="list" />
                    <?php if (isset($_REQUEST['my-items']) && $_REQUEST['my-items'] == true) : ?>
                        <input type="hidden" name="my-items" value="true" />
                    <?php endif ?>
                    <div class="field has-addons">
                        <div class="control">
                            <input class="input is-primary" type="search" placeholder="Suchen..." aria-label="Suchen" name="search_term" id="search_term" value="<?php echo isset($_REQUEST['search_term']) ? $_GET['search_term'] : '' ?>">
                        </div>
                        <div class="control">
                            <button class="button is-primary" type="submit">Suchen</button>
                        </div>
                    </div>
                </form>
            </div>
            <?php if (!isset($_SESSION['loggedin']) or !$_SESSION['loggedin']) : ?>
                <div class="navbar-item">
                    <div class="buttons">
                        <a class="button is-primary" href="index.php?page=register">
                            <strong>Registrieren</strong>
                        </a>
                        <a class="button is-light" href="index.php?page=login">
                            <strong>Login</strong>
                        </a>
                    </div>
                </div>
            <?php else : ?>
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link">
                        <div class="topbar-component-userprofile-picture-box mr-3">
                            <?php if ($filename !== '') : ?>
                                <img class="topbar-component-userprofile-picture" src="./assets/uploads/<?php echo $filename ?>" alt="user-profile-picture">
                            <?php else : ?>
                                <img class="topbar-component-userprofile-picture" src="./assets/placeholder.jpg" alt="item-picture">
                            <?php endif ?>
                        </div>
                        <div>
                            <?php echo $_SESSION['username'] ?>
                        </div>
                    </a>

                    <div class="navbar-dropdown is-right">
                        <a class="navbar-item" href="index.php?page=profile">
                            User Profil
                        </a>
                        <a class="navbar-item" href="index.php?page=bought-items">
                            Einkäufe
                        </a>
                        <hr class="navbar-divider">
                        <div class="navbar-item">
                            <a class="navbar-item" href="index.php?page=logout">
                                Logout
                            </a>
                        </div>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</nav>