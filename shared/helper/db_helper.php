<?php

function create_attachment(string $filename, string $old_filename): int
{
    global $mysqli, $error;
    $query = "INSERT INTO attachment (filename, old_filename) VALUES (?,?)";
    $stmt = $mysqli->prepare($query);
    if ($stmt === false) {
        $error .= 'prepare() failed ' . $mysqli->error . '<br />';
    }
    if (!$stmt->bind_param('ss', $filename, $old_filename)) {
        $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
    }
    $stmt->execute();
    return $mysqli->insert_id;
}

function delete_attachment(int $id)
{
    global $mysqli, $error;
    $attachment_not_found = false;
    $query = "SELECT * FROM attachment WHERE id=?";
    $stmt = $mysqli->prepare($query);
    if ($stmt === false) {
        $error .= 'prepare() failed ' . $mysqli->error . '<br />';
    }
    if (!$stmt->bind_param('i', $id)) {
        $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
    }
    if (!$stmt->execute()) {
        $error .= 'execute() failed ' . $mysqli->error . '<br />';
    }
    $result = $stmt->get_result();
    if ($result->num_rows === 0) {
        $attachment_not_found = true;
    } else {
        $result = $result->fetch_assoc();
    }
    if (!$attachment_not_found) {
        $query = "DELETE FROM attachment WHERE id=?";
        $stmt = $mysqli->prepare($query);
        if ($stmt === false) {
            $error .= 'prepare() failed ' . $mysqli->error . '<br />';
        }
        if (!$stmt->bind_param('i', $id)) {
            $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
        }
        if (!$stmt->execute()) {
            $error .= 'execute() failed ' . $mysqli->error . '<br />';
        }
        $base = "assets/uploads/";
        if (empty($error)) {
            unlink($base . $result['filename']);
        }
    }
}
