<?php
function validate_and_create_attachment()
{
    global $error, $stored_filename;
    if (!empty($_FILES["imageUpload"]["name"])) {
        $targetDir = "assets/uploads/";
        $filename   = uniqid() . "-" . time();
        $extension  = pathinfo($_FILES["imageUpload"]["name"], PATHINFO_EXTENSION);
        $basename   = $filename . "." . $extension;
        $targetFilePath = $targetDir . $basename;
        $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);
        $allowTypes = array('jpg', 'png', 'jpeg');
        if (in_array($fileType, $allowTypes)) {
            if (move_uploaded_file($_FILES["imageUpload"]["tmp_name"], $targetFilePath)) {
                $stored_filename = $basename;
            } else {
                $error .= "Sorry, there was an error uploading your file.<br />";
            }
        } else {
            $error .= 'Sorry, only JPG, JPEG, PNG, GIF, & PDF files are allowed to upload.<br />';
        }
    }
}
