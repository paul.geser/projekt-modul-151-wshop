<?php
$message = $error = $name = $short_description = $long_description = $stored_filename = '';
$price = 0;
$page_mode = 'create';
$picture_exists = false;

function validate_input()
{
    global $error, $name, $short_description, $long_description, $price, $stored_filename;
    if (isset($_POST['name'])) {
        $name = htmlentities(strip_tags(trim($_POST['name'])));

        if (empty($name) || !preg_match("/(?=^.{1,100}$).*$/", $name)) {
            $error .= "Geben Sie bitte einen korrekten Wert für den Produktnamen ein.<br />";
        }
    } else {
        $error .= "Geben Sie bitte einen Produktnamen ein.<br />";
    }

    if (isset($_POST['short_description'])) {
        $short_description = htmlentities(strip_tags(trim($_POST['short_description'])));

        if (empty($short_description) || !preg_match("/(?=^.{1,500}$).*$/", $short_description)) {
            $error .= "Geben Sie bitte einen korrekten Wert für die Kurzbeschreibung ein.<br />";
        }
    } else {
        $error .= "Geben Sie bitte eine kurz Beschreibung ein.<br />";
    }

    if (isset($_POST['long_description'])) {
        $long_description = htmlentities(strip_tags(trim($_POST['long_description'])));
    } else {
        $long_description = '';
    }

    if (isset($_POST['price'])) {
        $price = $_POST['price'];

        if (empty($price) || $price === 0 || $price < 0) {
            $error .= "Geben Sie bitte einen korrekten Wert für den Produktpreis ein.<br />";
        }
    } else {
        $error .= "Geben Sie bitte einen Produktpreis ein.<br />";
    }

    validate_and_create_attachment();
}
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true) {
    if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_GET['mode']) && $_GET['mode'] === 'create' && isset($_POST['submitted']) && $_POST['submitted'] == true) {
        validate_input();

        if (empty($error)) {
            $mysqli->begin_transaction();
            if ($stored_filename !== '') {
                $attachment_id = create_attachment($stored_filename, $_FILES["imageUpload"]["name"]);

                $query = "INSERT INTO product (name, short_description, long_description, price, creation_date, owner, picture_id) values (?,?,?,?,?,?,?)";
                $stmt = $mysqli->prepare($query);

                if ($stmt === false) {
                    $error .= 'prepare() failed ' . $mysqli->error . '<br />';
                }
                $date = date("Y-m-d");
                if (!$stmt->bind_param('sssdsii', $name, $short_description, $long_description, $price, $date, $_SESSION['id'], $attachment_id)) {
                    $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
                }
            } else {
                $query = "INSERT INTO product (name, short_description, long_description, price, creation_date, owner) values (?,?,?,?,?,?)";
                $stmt = $mysqli->prepare($query);

                if ($stmt === false) {
                    $error .= 'prepare() failed ' . $mysqli->error . '<br />';
                }
                $date = date("Y-m-d");
                if (!$stmt->bind_param('sssdsi', $name, $short_description, $long_description, $price, $date, $_SESSION['id'])) {
                    $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
                }
            }

            if (!$stmt->execute()) {
                $error .= 'execute() failed ' . $mysqli->error . '<br />';
            }
            $result = $stmt->get_result();
            $new_product_id = $mysqli->insert_id;

            $mysqli->commit();

            if (empty($error)) {
                header('Location: index.php?page=detail-view&item-id=' . $new_product_id);
                exit();
            }
        }
    } elseif (isset($_REQUEST['id'])) {
        $page_mode = 'edit';
        if (isset($_POST['submitted']) && $_POST['submitted'] == true) {
            validate_input();
            if (empty($error)) {
                // start transaction
                $mysqli->begin_transaction();
                if ($stored_filename !== '') {
                    $attachment_id = create_attachment($stored_filename, $_FILES["imageUpload"]["name"]);
                    unset($_SESSION['user_picture_name']);
                    $_SESSION['user_picture_name'] = $stored_filename;
                    $query = "UPDATE product set name=?, short_description=?, long_description=?, price=?, picture_id=? WHERE id=?";
                    $stmt = $mysqli->prepare($query);

                    if ($stmt === false) {
                        $error .= 'prepare() failed ' . $mysqli->error . '<br />';
                    }
                    if (!$stmt->bind_param('sssdii', $name, $short_description, $long_description, $price, $attachment_id, $_REQUEST['id'])) {
                        $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
                    }
                } else {
                    $query = "UPDATE product set name=?, short_description=?, long_description=?, price=? WHERE id=?";
                    $stmt = $mysqli->prepare($query);

                    if ($stmt === false) {
                        $error .= 'prepare() failed ' . $mysqli->error . '<br />';
                    }
                    if (!$stmt->bind_param('sssdi', $name, $short_description, $long_description, $price, $_REQUEST['id'])) {
                        $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
                    }
                }
                if (!$stmt->execute()) {
                    $error .= 'execute() failed ' . $mysqli->error . '<br />';
                }
                $result = $stmt->get_result();
                if (isset($_REQUEST['old_picture_id'])) {
                    delete_attachment($_REQUEST['old_picture_id']);
                }

                $mysqli->commit();
                if (empty($error)) {
                    header('Location: index.php?page=detail-view&item-id=' . $_REQUEST['id']);
                    exit();
                }
            }
        } else {
            $item_id = $_REQUEST['id'];

            $query = "SELECT * FROM product where id =?";
            $stmt = $mysqli->prepare($query);
            if ($stmt === false) {
                $error .= 'prepare() failed ' . $mysqli->error . '<br />';
            }
            if (!$stmt->bind_param('i', $item_id)) {
                $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
            }
            if (!$stmt->execute()) {
                $error .= 'execute() failed ' . $mysqli->error . '<br />';
            }
            $result = $stmt->get_result();
            $stmt->close();

            if ($result->num_rows === 0) {
                $item_not_found = true;
            } else {
                $item_not_found = false;
                $result = $result->fetch_assoc();
                $id = $result['id'];
                $name = $result['name'];
                $short_description = $result['short_description'];
                $long_description = $result['long_description'];
                $price = $result['price'];
                if ($result['picture_id']) {
                    $picture_exists = true;
                }
            }
        }
    }
} else {
    $error .= 'Sie sind nicht eingeloggt!';
}
?>
<div class="container">
    <div class="card">
        <div class="card-content">
            <?php
            if (!empty($error)) {
                echo "<div class=\"notification is-danger\" role=\"alert\">" . $error . "</div>";
            }
            ?>
            <form action="?page=edit-item&<?php
                                            if ($page_mode === 'create') {
                                                echo 'mode=create';
                                            } else {
                                                echo 'id=' . $_REQUEST['id'];
                                            }
                                            ?>" method="post" enctype="multipart/form-data">
                <?php if (isset($_REQUEST['mode']) && $_REQUEST['mode'] === 'create') : ?>
                    <div class="is-size-2">Neues Produkt erstellen</div>
                <?php else : ?>
                    <div class="is-size-2">Produkt editieren</div>
                <?php endif ?>

                <div class="field">
                    <label class="label">Produkt Name</label>
                    <div class="control">
                        <input type="text" name="name" class="input" id="name" pattern="(?=^.{1,100}$).*$" placeholder="min 1 Zeichen, max. 100 Zeichen" title="min 1 Zeichen, max. 100 Zeichen" value="<?php echo $name ?>" required="true" />
                    </div>
                </div>

                <div class="field">
                    <label class="label">Kurze Beschreibung</label>
                    <div class="control">
                        <textarea name="short_description" class="textarea" id="short_description" placeholder="min 1 Zeichen, max. 500 Zeichen" title="min 1 Zeichen, max. 500 Zeichen" required="true" rows="2" minlength="1" maxlength="500"><?php echo $short_description ?></textarea>
                    </div>
                </div>

                <div class="field">
                    <label class="label">Lange Beschreibung</label>
                    <div class="control">
                        <textarea type="text" name="long_description" class="textarea" id="long_description" placeholder="optional" rows="3"><?php echo $long_description ?></textarea>
                    </div>
                </div>

                <div class="field">
                    <label class="label">Preis</label>
                    <div class="control">
                        <input type="number" name="price" class="input" id="price" step=".01" min="1" placeholder="min. 1 Nummer, Wert grösser als 0" title="min. 1 Nummer, Wert grösser als 0" value="<?php echo $price ?>" required="true" />
                    </div>
                </div>

                <div class="field">
                    <?php if ($picture_exists) : ?>
                        <label class="label">Neues Produktbild</label>
                    <?php else : ?>
                        <label class="label">Produktbild</label>
                    <?php endif ?>
                    <div class="file is-boxed">
                        <label class="file-label">
                            <input class="file-input" type="file" name="imageUpload" id="imageUpload" accept=".jpg,.png,.jpeg" />
                            <span class="file-cta">
                                <span class="file-icon">
                                    <i class="fas fa-upload"></i>
                                </span>
                                <span class="file-label">
                                    Laden Sie hier das Bild hoch…
                                </span>
                            </span>
                        </label>
                    </div>
                    <div class="is-size-7">Nur .JPG, .PNG und .JPEG Formate werden akzeptiert.</div>
                </div>

                <input type="hidden" name="submitted" value="true" />
                <?php if ($picture_exists) : ?>
                    <input type="hidden" name="old_picture_id" value="<?php echo $result['picture_id'] ?>" />
                <?php endif ?>
                <div>
                    <button type="submit" name="button" value="submit" class="button is-primary">Produkt <?php
                                                                                                            if ($page_mode === 'create') {
                                                                                                                echo 'erstellen';
                                                                                                            } else {
                                                                                                                echo 'speichern';
                                                                                                            }
                                                                                                            ?></button>
                    <a class="button is-light" href="?page=list">
                        Zurück
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>