<?php

$error = $message =  '';
$firstname = $lastname = $email = $username = '';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_REQUEST['mode']) && $_REQUEST['mode'] === 'submitted') {

    if (isset($_POST['firstname'])) {
        $firstname = htmlentities(strip_tags(trim($_POST['firstname'])));

        if (empty($firstname) || strlen($firstname) > 45) {
            $error .= "Geben Sie bitte einen korrekten Vornamen ein.<br />";
        }
    } else {
        $error .= "Geben Sie bitte einen Vornamen ein.<br />";
    }

    if (isset($_POST['lastname'])) {
        $lastname = htmlentities(strip_tags(trim($_POST['lastname'])));

        if (empty($lastname) || strlen($lastname) > 45) {
            $error .= "Geben Sie bitte einen korrekten Nachnamen ein.<br />";
        }
    } else {
        $error .= "Geben Sie bitte einen Nachnamen ein.<br />";
    }

    if (isset($_POST['username'])) {
        $username = htmlentities(strip_tags(trim($_POST['username'])));

        if (empty($username) || !preg_match("/(?=.*[a-z])(?=.*[A-Z])[a-zA-Z]{6,30}/", $username)) {
            $error .= "Geben Sie bitte einen korrekten Usernamen ein.<br />";
        }
    } else {
        $error .= "Geben Sie bitte einen Username ein.<br />";
    }

    if (isset($_POST['email'])) {
        $email = htmlentities(strip_tags(trim($_POST['email'])));

        if (strlen($email) > 60 || !empty($email) && !preg_match("/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4}/", $email)) {
            $error .= "Geben Sie bitte eine korrekte E-Mail ein.<br />";
        }
    } else {
        $error .= "Geben Sie bitte eine E-Mail ein.<br />";
    }

    if (isset($_POST['password'])) {
        $password = htmlentities(strip_tags(trim($_POST['password'])));

        if (empty($password) || !preg_match("/(?=^.{8,255}$)((?=.*\d+)(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/", $password)) {
            $error .= "Geben Sie bitte einen korrektes Password ein.<br />";
        }
    } else {
        $error .= "Geben Sie bitte ein Password ein.<br />";
    }

    if (empty($error)) {
        $query = "SELECT * FROM user where username=? ";
        $stmt = $mysqli->prepare($query);

        if ($stmt === false) {
            $error .= 'prepare() failed ' . $mysqli->error . '<br />';
        }
        if (!$stmt->bind_param('s', $username)) {
            $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
        }
        if (!$stmt->execute()) {
            $error .= 'execute() failed ' . $mysqli->error . '<br />';
        }
        $result = $stmt->get_result();
        $stmt->close();
        if ($result->num_rows !== 0) {
            $error .= 'Benutzername existiert schon, bitte wählen Sie einen anderen!<br />';
        } else {
            $query = "INSERT INTO user (prename, lastname, email, username, password) values (?,?,?,?,?)";
            $stmt = $mysqli->prepare($query);

            if ($stmt === false) {
                $error .= 'prepare() failed ' . $mysqli->error . '<br />';
            }
            if (!$stmt->bind_param('sssss', $firstname, $lastname, $email, $username, password_hash($password, PASSWORD_DEFAULT))) {
                $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
            }
            if (!$stmt->execute()) {
                $error .= 'execute() failed ' . $mysqli->error . '<br />';
            }
            $stmt->close();

            if (empty($error)) {
                header('Location: index.php?page=login');
            }
        }
    }
}
?>

<div class="container">
    <div class="card">
        <div class="card-content">
            <h1 class="is-size-2">Registrierung</h1>
            <p class="is-size-5">
                Bitte registrieren Sie sich, damit Sie WShop im vollen Umfang benutzen können.
            </p>
            <?php
            if (!empty($error)) {
                echo "<div class=\"notification is-danger\" role=\"alert\">" . $error . "</div>";
            } else if (!empty($message)) {
                echo "<div class=\"notification is-success\" role=\"alert\">" . $message . "</div>";
            }
            ?>
            <form action="?page=register&mode=submitted" method="post">
                <div class="field">
                    <label class="label" for="firstname">Vorname *</label>
                    <div class="control has-icons-left">
                        <input type="text" name="firstname" class="input" id="firstname" value="<?php echo $firstname ?>" placeholder="Geben Sie Ihren Vornamen an." maxlength="45" required="true">
                        <span class="icon is-small is-left">
                            <i class="fas fa-user"></i>
                        </span>
                    </div>
                </div>

                <div class="field">
                    <label class="label" for="lastname">Nachname *</label>
                    <div class="control has-icons-left">
                        <input type="text" name="lastname" class="input" id="lastname" value="<?php echo $lastname ?>" placeholder="Geben Sie Ihren Nachnamen an" maxlength="45" required="true">
                        <span class="icon is-small is-left">
                            <i class="fas fa-user"></i>
                        </span>
                    </div>
                </div>

                <div class="field">
                    <label class="label" for="email">Email</label>
                    <div class="control has-icons-left">
                        <input type="email" name="email" class="input" id="email" pattern="[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4}" value="<?php echo $email ?>" placeholder="Geben Sie Ihre E-Mail an" title="Geben Sie eine korrekte E-Mail an" maxlength="60">
                        <span class="icon is-small is-left">
                            <i class="fas fa-envelope"></i>
                        </span>
                    </div>
                </div>

                <div class="field">
                    <label class="label" for="username">Benutzername *</label>
                    <div class="control has-icons-left">
                        <input type="text" name="username" class="input" id="username" value="<?php echo $username ?>" placeholder="Gross- und Keinbuchstaben, min. 6 Zeichen, max. 30 Zeichen" pattern="(?=.*[a-z])(?=.*[A-Z])[a-zA-Z]{6,}" title="Gross- und Keinbuchstaben, min. 6 Zeichen, max. 30 Zeichen" maxlength="30" required="true">
                        <span class="icon is-small is-left">
                            <i class="fas fa-user"></i>
                        </span>
                    </div>
                </div>

                <div class="field">
                    <label class="label" for="password">Password *</label>
                    <div class="control has-icons-left">
                        <input type="password" name="password" class="input" id="password" placeholder="Gross- und Kleinbuchstaben, Zahlen, Sonderzeichen, min. 8 Zeichen, max. 255 Zeichen keine Umlaute" pattern="(?=^.{8,}$)((?=.*\d+)(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="mindestens einen Gross-, einen Kleinbuchstaben, eine Zahl und ein Sonderzeichen, mindestens 8 Zeichen, maximal. 255 Zeichen,keine Umlaute." maxlength="255" required="true">
                        <span class="icon is-small is-left">
                            <i class="fas fa-lock"></i>
                        </span>
                    </div>
                </div>
                <button type="submit" name="button" value="submit" class="button is-primary">Senden</button>
                <button type="reset" name="button" value="reset" class="button is-light">Löschen</button>
            </form>
        </div>
    </div>
</div>