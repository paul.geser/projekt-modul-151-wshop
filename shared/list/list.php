<div class="container">
    <div class="card">
        <div class="card-content">
            <?php
            $error = '';
            $my_items = false;
            if (isset($_REQUEST['search_term'])) {
                $search_term = htmlentities(strip_tags($_REQUEST['search_term']));
                $search_term = $search_term . '%';
            } else {
                $search_term = '%';
            }
            if (isset($_REQUEST['my-items'])) {
                if (isset($_SESSION['loggedin']) and $_SESSION['loggedin']) {
                    $query = 'SELECT p.*, a.filename, a.old_filename, u.username, ua.filename AS owner_filename
                    FROM product p
                    LEFT JOIN attachment a ON p.picture_id = a.id
                    LEFT JOIN user u ON p.owner = u.id
                    LEFT JOIN attachment ua ON u.picture_id = ua.id
                    WHERE p.owner=? AND p.bought=0 AND name LIKE ?
                    ORDER BY p.id DESC';
                    $stmt = $mysqli->prepare($query);
                    if ($stmt === false) {
                        $error .= 'prepare() failed ' . $mysqli->error . '<br />';
                    }
                    $id = $_SESSION['id'];
                    if (!$stmt->bind_param('is', $id, $search_term)) {
                        $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
                    }
                    $my_items = true;
                } else {
                    $error .= "Sie sind nicht eingeloggt, bitte loggen Sie sich ein!<br />";
                }
            } else {
                $query = "SELECT p.*, a.filename, a.old_filename, u.username, ua.filename AS owner_filename
                FROM product p
                LEFT JOIN attachment a ON p.picture_id = a.id
                LEFT JOIN user u ON p.owner = u.id
                LEFT JOIN attachment ua ON u.picture_id = ua.id
                WHERE p.bought=0 AND p.name LIKE ?
                ORDER BY p.id DESC";
                $stmt = $mysqli->prepare($query);
                if ($stmt === false) {
                    $error .= 'prepare() failed ' . $mysqli->error . '<br />';
                }
                if (!$stmt->bind_param('s', $search_term)) {
                    $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
                }
            }
            if (empty($error)) {
                if (!$stmt->execute()) {
                    $error .= 'execute() failed ' . $mysqli->error . '<br />';
                }
                $result = $stmt->get_result();
                $stmt->close();
            ?>
                <?php if ($search_term !== '%') : ?>
                    <div class="is-size-5 mb-2">Folgende Produkte wurden mit dem Suchbegriff <strong><?php echo str_replace('%', '', $search_term); ?></strong> gefunden:</div>
                <?php endif ?>
                <?php if ($result->num_rows === 0) : ?>
                    <div class="notification is-warning is-light">Keine Produkte wurden gefunden...</div>
                <?php else : ?>
                    <div class="list-component-product-divider"></div>
                    <?php while ($row = $result->fetch_assoc()) : ?>
                        <a class="list-component-product-list-item-link" href="?page=detail-view&item-id=<?php echo $row['id'] ?>">
                            <div class="list-component-product-list-item">
                                <div class="list-component-product-list-item-picture-box">
                                    <?php if ($row['picture_id']) : ?>
                                        <img src="./assets/uploads/<?php echo $row['filename'] ?>" alt="<?php echo $row['old_filename'] ?>" class="list-component-product-list-item-picture">
                                    <?php else : ?>
                                        <img src="./assets/placeholder.jpg" alt="product-default-picture" class="list-component-product-list-item-picture">
                                    <?php endif ?>
                                </div>
                                <div class="list-component-product-list-item-content-box">
                                    <?php if (isset($_SESSION['id']) && !$my_items && $row['owner'] === $_SESSION['id']) : ?>
                                        <span class="tag is-primary is-light">Dieses Produkt gehört dir!</span>
                                    <?php endif ?>
                                    <?php if (is_decimal($row['price'])) : ?>
                                        <div class="has-text-danger is-size-4"><strong><?php echo $row['price'] ?></strong></div>
                                    <?php else : ?>
                                        <div class="has-text-danger is-size-4"><strong><?php echo $row['price'] ?>.-</strong></div>
                                    <?php endif ?>
                                    <div class="is-size-3"><strong><?php echo $row['name'] ?></strong></div>
                                    <div class="is-size-5"><?php echo $row['short_description'] ?></div>
                                    <div class="has-text-grey"><?php echo $row['creation_date'] ?></div>
                                    <div class="is-flex is-flex-direction-row is-align-items-center">
                                        <div class="item-detailview-component-picture-box">
                                            <?php if ($row['owner_filename']) : ?>
                                                <img src="./assets/uploads/<?php echo $row['owner_filename'] ?>" alt="owner_picture" class="item-detailview-component-picture">
                                            <?php else : ?>
                                                <img src="./assets/placeholder.jpg" alt="item-picture" class="item-detailview-component-picture">
                                            <?php endif ?>
                                        </div>
                                        <div class="ml-3"><?php echo $row['username'] ?></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="list-component-product-divider"></div>
            <?php endwhile;
                endif;
            } else {
                echo "<div class=\"alert alert-danger\" role=\"alert\">" . $error . "</div>";
            }
            ?>
        </div>
    </div>
</div>