<?php

$error = '';
$message = '';

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_REQUEST['mode']) && $_REQUEST['mode'] === 'submitted') {

    if (isset($_POST['username'])) {

        $username = htmlentities(strip_tags(trim($_POST['username'])));

        if (empty($username) || !preg_match("/(?=.*[a-z])(?=.*[A-Z])[a-zA-Z]{6,30}/", $username)) {
            $error .= "Der Benutzername entspricht nicht dem geforderten Format.<br />";
        }
    } else {
        $error .= "Geben Sie bitte den Benutzername an.<br />";
    }

    if (isset($_POST['password'])) {

        $password = htmlentities(strip_tags(trim($_POST['password'])));
        if (empty($password) || !preg_match("/(?=^.{8,255}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/", $password)) {
            $error .= "Das Passwort entspricht nicht dem geforderten Format.<br />";
        }
    } else {
        $error .= "Geben Sie bitte das Passwort an.<br />";
    }

    if (empty($error)) {
        $query = 'SELECT u.*, a.filename, a.old_filename
        FROM user u LEFT JOIN attachment a
        ON u.picture_id = a.id
        where u.username =?';
        $stmt = $mysqli->prepare($query);
        if ($stmt === false) {
            $error .= 'prepare() failed ' . $mysqli->error . '<br />';
        }
        if (!$stmt->bind_param('s', $username)) {
            $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
        }
        if (!$stmt->execute()) {
            $error .= 'execute() failed ' . $mysqli->error . '<br />';
        }

        $result = $stmt->get_result();
        $stmt->close();

        if ($result->num_rows === 0) {
            $error .= 'Benutzer existiert nicht!<br />';
        } else {
            $result = $result->fetch_assoc();
            if (password_verify($password, $result['password'])) {
                $message .= "Sie sind nun eingeloggt";
                $_SESSION['loggedin'] = true;
                $_SESSION['username'] = $result['username'];
                $_SESSION['id'] = $result['id'];
                session_regenerate_id();
                header('Location: index.php?page=home');

                exit();
            } else {
                $error .= "Passwort ist falsch";
            }
        }
    }
}

?>

<div class="container">
    <div class="card">
        <div class="card-content">
            <h1 class="is-size-2">Login</h1>
            <p class="is-size-5">
                Bitte melden Sie sich mit Benutzernamen und Passwort an.
            </p>
            <?php
            if (!empty($message)) {
                echo "<div class=\"notification is-success\" role=\"alert\">" . $message . "</div>";
            } else if (!empty($error)) {
                echo "<div class=\"notification is-danger\" role=\"alert\">" . $error . "</div>";
            }
            ?>
            <form action="?page=login&mode=submitted" method="POST">
                <div class="field">
                    <label class="label" for="username">Benutzername *</label>
                    <div class="control has-icons-left">
                        <input type="text" name="username" class="input" id="username" value="" placeholder="Gross- und Keinbuchstaben, min 6 Zeichen, max. 30 Zeichen" pattern="(?=.*[a-z])(?=.*[A-Z])[a-zA-Z]{6,30}" title="Gross- und Keinbuchstaben, min 6 Zeichen, max. 30 Zeichen" required="true">
                        <span class="icon is-small is-left">
                            <i class="fas fa-user"></i>
                        </span>
                    </div>
                </div>
                <div class="field">
                    <label class="label" for="password">Password *</label>
                    <div class="control has-icons-left">
                        <input type="password" name="password" class="input" id="password" placeholder="Gross- und Kleinbuchstaben, Zahlen, Sonderzeichen, min. 8 Zeichen, max. 255 Zeichen, keine Umlaute" pattern="(?=^.{8,255}$)((?=.*\d+)(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="mindestens einen Gross-, einen Kleinbuchstaben, eine Zahl und ein Sonderzeichen, mindestens 8 Zeichen lang, maximal 255 Zeichen lang, keine Umlaute." required="true">
                        <span class="icon is-small is-left">
                            <i class="fas fa-lock"></i>
                        </span>
                    </div>
                </div>
                <button type="submit" name="button" value="submit" class="button is-primary">Senden</button>
                <button type="reset" name="button" value="reset" class="button is-light">Löschen</button>
            </form>
        </div>
    </div>
</div>