<div class="container">
    <div class="card">
        <div class="card-content">
            <div class="is-size-2">Einkäufe</div>
            <?php
            if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true) {
                $error = '';
                $query = "SELECT p.*, a.filename, a.old_filename
                FROM product p LEFT JOIN attachment a
                ON p.picture_id = a.id
                WHERE p.bought=1 AND p.new_owner=?";
                $stmt = $mysqli->prepare($query);
                if ($stmt === false) {
                    $error .= 'prepare() failed ' . $mysqli->error . '<br />';
                }
                $id = $_SESSION['id'];
                if (!$stmt->bind_param('i', $id)) {
                    $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
                }
                if (!$stmt->execute()) {
                    $error .= 'execute() failed ' . $mysqli->error . '<br />';
                }
                $result = $stmt->get_result();
                $stmt->close();
            } else {
                $error .= 'Sie sind nicht eingeloggt!';
            }
            if (empty($error)) : ?>
                <div class="list-group">
                    <div class="list-component-product-divider"></div>
                    <?php if ($result->num_rows !== 0) : ?>
                        <?php while ($row = $result->fetch_assoc()) : ?>
                            <a href="index.php?page=detail-view&item-id=<?php echo $row['id'] ?>" class="list-group-item list-group-item-action">
                                <div class="bought-items-compoents-list-item-content is-flex is-flex-direction-row is-align-items-center is-justify-content-space-between">
                                    <div class="is-flex is-align-items-center">
                                        <div class="bought-items-component-product-list-item-picture-box">
                                            <?php if ($row['picture_id']) : ?>
                                                <img src="./assets/uploads/<?php echo $row['filename'] ?>" alt="<?php echo $row['old_filename'] ?>" class="bought-items-component-product-list-item-picture">
                                            <?php else : ?>
                                                <img src="./assets/placeholder.jpg" alt="product-default-picture" class="bought-items-component-product-list-item-picture">
                                            <?php endif ?>
                                        </div>
                                        <div class="is-size-4 ml-3"><strong class="has-text-black"><?php echo $row['name'] ?></strong></div>
                                    </div>
                                    <div>
                                        <p class="is-size-4 has-text-danger"><?php echo $row['price'] ?>.-</p>
                                    </div>
                                </div>
                            </a>
                            <div class="list-component-product-divider"></div>
                        <?php endwhile ?>
                    <?php else : ?>
                        <p class="is-size-5 mt-6">Sie haben noch keine Einkäufe getätigt!</p>
                        <p class="is-size-5 mb-6 mt-2">Gehen Sie auf <strong>Alle Produkte</strong> und kaufen Sie ihr erstes Produkt.</p>
                        <div class="list-component-product-divider"></div>
                    <?php endif ?>
                </div>
            <?php else : ?>
                <div class="notification is-danger" role="alert"><?php echo $error ?></div>
            <?php endif ?>
        </div>
    </div>
</div>