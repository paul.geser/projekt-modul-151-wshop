<?php
$error = '';
$item_not_found = false;
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true) {
    if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['submitted']) && $_POST['submitted'] == true) {
        $query = "UPDATE product set bought=1, new_owner=? WHERE id=?";
        $stmt = $mysqli->prepare($query);

        if ($stmt === false) {
            $error .= 'prepare() failed ' . $mysqli->error . '<br />';
        }

        // Daten an das SQL-Statement binden
        if (!$stmt->bind_param('ii', $_SESSION['id'], $_REQUEST['item-id'])) {
            $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
        }

        // SQL-Statement ausführen
        if (!$stmt->execute()) {
            $error .= 'execute() failed ' . $mysqli->error . '<br />';
        }
        $result = $stmt->get_result();

        $stmt->close();
        if (empty($error)) {
            header('Location: index.php?page=bought-items');
            exit();
        }
    } elseif (isset($_REQUEST['item-id'])) {
        $item_id = $_REQUEST['item-id'];

        $query = "SELECT p.*, a.filename, a.old_filename
                FROM product p LEFT JOIN attachment a
                ON p.picture_id = a.id
                where p.id=?";
        $stmt = $mysqli->prepare($query);
        if ($stmt === false) {
            $error .= 'prepare() failed ' . $mysqli->error . '<br />';
        }
        if (!$stmt->bind_param('i', $item_id)) {
            $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
        }
        if (!$stmt->execute()) {
            $error .= 'execute() failed ' . $mysqli->error . '<br />';
        }
        $result = $stmt->get_result();
        $stmt->close();

        if ($result->num_rows === 0) {
            $item_not_found = true;
        } else {
            $item_not_found = false;
            $result = $result->fetch_assoc();
            $id = $result['id'];
            $name = $result['name'];
            $short_description = $result['short_description'];
            $long_description = $result['long_description'];
            $price = $result['price'];
        }
    }
} else {
    $error .= 'Sie sind nicht eingeloggt!';
}
?>
<div class="container">
    <div class="card">
        <div class="card-content">
            <?php
            if (!empty($error)) {
                echo "<div class=\"alert alert-danger\" role=\"alert\">" . $error . "</div>";
            }
            ?>
            <?php if (!$item_not_found) : ?>
                <form action="?page=buy&item-id=<?php echo $result['id'] ?>" method="post">
                <?php else : ?>
                    <form method="post">
                    <?php endif ?>
                    <div class="is-size-2">Kaufbestätigung</div>
                    <?php if ($item_not_found) : ?>
                        <div class="is-size-3">Das zu kaufende Produkt konnte nicht gefunden werden</div>
                    <?php else : ?>
                        <div class="is-size-4">Sind Sie sich sicher das sie folgendes Produkt kaufen wollen?</div>
                        <div>
                            <div class="list-component-product-divider"></div>
                            <div class="item-buy-component-product-list-item">
                                <div class="list-component-product-list-item-picture-box">
                                    <?php if ($result['picture_id']) : ?>
                                        <img src="./assets/uploads/<?php echo $result['filename'] ?>" alt="<?php echo $result['old_filename'] ?>" class="list-component-product-list-item-picture">
                                    <?php else : ?>
                                        <img src="./assets/placeholder.jpg" alt="product-default-picture" class="list-component-product-list-item-picture">
                                    <?php endif ?>
                                </div>
                                <div class="list-component-product-list-item-content-box">
                                    <div class="has-text-danger is-size-4"><strong class="has-text-danger"><?php echo $result['price'] ?>.-</strong></div>
                                    <div class="is-size-3"><strong><?php echo $result['name'] ?></strong></div>
                                    <div class="is-size-5"><?php echo $result['short_description'] ?></div>
                                    <div class="has-text-grey"><?php echo $result['creation_date'] ?></div>
                                </div>
                            </div>
                            <div class="list-component-product-divider"></div>
                        </div>
                    <?php endif ?>
                    <div class="buttons mt-4">
                        <?php if (!$item_not_found) : ?>
                            <button type="submit" name="button" value="submit" class="button is-primary">Kaufen</button>
                        <?php endif ?>
                        <a class="button is-light" href="?page=detail-view&item-id=<?php echo $result['id'] ?>">
                            Zurück
                        </a>
                    </div>
                    <input type="hidden" name="submitted" value="true" />
                    </form>
        </div>
    </div>
</div>