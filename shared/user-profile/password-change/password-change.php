<?php
$error = $message = $old_password = $new_password = $confirm_new_password = '';
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true) {
    if ($_SERVER['REQUEST_METHOD'] == "POST" && !isset($_REQUEST['link'])) {
        if (isset($_POST['old_password'])) {
            $old_password = htmlentities(strip_tags(trim($_POST['old_password'])));

            if (empty($old_password) || !preg_match("/(?=^.{8,255}$)((?=.*\d+)(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/", $old_password)) {
                $error .= "Geben Sie bitte einen korrektes Password für Ihr altes Passwort ein.<br />";
            }
        } else {
            $error .= "Geben Sie bitte Ihr altes Password ein.<br />";
        }

        if (isset($_POST['new_password'])) {
            $new_password = htmlentities(strip_tags(trim($_POST['new_password'])));

            if (empty($new_password) || !preg_match("/(?=^.{8,255}$)((?=.*\d+)(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/", $new_password)) {
                $error .= "Geben Sie bitte einen korrektes neues Passwort ein.<br />";
            }
        } else {
            $error .= "Geben Sie bitte Ihr neues Password ein.<br />";
        }

        if (isset($_POST['confirm_new_password'])) {
            $confirm_new_password = htmlentities(strip_tags(trim($_POST['confirm_new_password'])));

            if (empty($confirm_new_password) || !preg_match("/(?=^.{8,255}$)((?=.*\d+)(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/", $confirm_new_password)) {
                $error .= "Geben Sie bitte einen korrektes Passwort für die Passwortbestätigung ein.<br />";
            }
        } else {
            $error .= "Geben Sie bitte Ihr Bestätigungspassword ein.<br />";
        }

        if (empty($error)) {
            if ($new_password !== $confirm_new_password) {
                $error .= "Das neue Passwort und das Bestätigunngspasswort stimmen nicht überein!";
            } else {
                $id = $_SESSION['id'];
                $query = "SELECT * FROM user WHERE id=?";
                $stmt = $mysqli->prepare($query);
                if ($stmt === false) {
                    $error .= 'prepare() failed ' . $mysqli->error . '<br />';
                }
                if (!$stmt->bind_param('i', $id)) {
                    $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
                }
                if (!$stmt->execute()) {
                    $error .= 'execute() failed ' . $mysqli->error . '<br />';
                }

                $result = $stmt->get_result();
                $stmt->close();

                $result = $result->fetch_assoc();
                if (password_verify($old_password, $result['password'])) {
                    $query = "UPDATE user set password=? WHERE id=?";
                    $stmt = $mysqli->prepare($query);
                    if ($stmt === false) {
                        $error .= 'prepare() failed ' . $mysqli->error . '<br />';
                    }
                    $hashed_password = password_hash($new_password, PASSWORD_DEFAULT);
                    if (!$stmt->bind_param('si', $hashed_password, $id)) {
                        $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
                    }
                    if (!$stmt->execute()) {
                        $error .= 'execute() failed ' . $mysqli->error . '<br />';
                    }

                    $result = $stmt->get_result();
                    $stmt->close();
                    $message .= "Passwort wurde erfolgreich geändert";
                    $old_password = $new_password = $confirm_new_password = '';
                } else {
                    $error .= "Altes Passwort ist falsch";
                }
            }
        }
    }
} else {
    $error .= 'Sie sind nicht eingeloggt!';
}
?>
<div class="container">
    <div class="card">
        <div class="card-content">
            <form action="?page=profile&mode=password" method="post">
                <div class="is-size-2">Benutzerpasswort ändern</div>

                <?php
                if (!empty($message)) {
                    echo "<div class=\"notification is-success\" role=\"alert\">" . $message . "</div>";
                } else if (!empty($error)) {
                    echo "<div class=\"notification is-danger\" role=\"alert\">" . $error . "</div>";
                }
                ?>

                <div class="field">
                    <label class="label">Altes Passwort</label>
                    <div class="control has-icons-left">
                        <input type="password" name="old_password" class="input" id="old_password" value="<?php echo $old_password ?>" placeholder="Gross- und Kleinbuchstaben, Zahlen, Sonderzeichen, min. 8 Zeichen, max. 255 Zeichen keine Umlaute" pattern="(?=^.{8,}$)((?=.*\d+)(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="mindestens einen Gross-, einen Kleinbuchstaben, eine Zahl und ein Sonderzeichen, mindestens 8 Zeichen, maximal. 255 Zeichen,keine Umlaute." maxlength="255" required="true" />
                        <span class="icon is-small is-left">
                            <i class="fas fa-lock"></i>
                        </span>
                    </div>
                </div>

                <div class="field">
                    <label class="label">Neues Passwort</label>
                    <div class="control has-icons-left">
                        <input type="password" name="new_password" class="input" id="new_password" value="<?php echo $new_password ?>" placeholder="Gross- und Kleinbuchstaben, Zahlen, Sonderzeichen, min. 8 Zeichen, max. 255 Zeichen keine Umlaute" pattern="(?=^.{8,}$)((?=.*\d+)(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="mindestens einen Gross-, einen Kleinbuchstaben, eine Zahl und ein Sonderzeichen, mindestens 8 Zeichen, maximal. 255 Zeichen,keine Umlaute." maxlength="255" required="true" />
                        <span class="icon is-small is-left">
                            <i class="fas fa-lock"></i>
                        </span>
                    </div>
                </div>

                <div class="field">
                    <label class="label">Neues Passwort bestätigen</label>
                    <div class="control has-icons-left">
                        <input type="password" name="confirm_new_password" class="input" id="confirm_new_password" value="<?php echo $confirm_new_password ?>" placeholder="Gross- und Kleinbuchstaben, Zahlen, Sonderzeichen, min. 8 Zeichen, max. 255 Zeichen keine Umlaute" pattern="(?=^.{8,}$)((?=.*\d+)(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="mindestens einen Gross-, einen Kleinbuchstaben, eine Zahl und ein Sonderzeichen, mindestens 8 Zeichen, maximal. 255 Zeichen,keine Umlaute." maxlength="255" required="true" />
                        <span class="icon is-small is-left">
                            <i class="fas fa-lock"></i>
                        </span>
                    </div>
                </div>

                <div>
                    <button type="submit" name="button" value="submit" class="button is-primary">Passwort ändern</button>
                    <a class="button is-light" href="?page=profile&mode=read">
                        Zurück
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>