<?php
$error = $username = '';
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true) {
    if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_REQUEST['submitted']) && $_REQUEST['submitted'] == true) {
        if (isset($_POST['username'])) {
            $username = htmlentities(strip_tags(trim($_POST['username'])));

            if (empty($username) || !preg_match("/(?=.*[a-z])(?=.*[A-Z])[a-zA-Z]{6,30}/", $username)) {
                $error .= "Geben Sie bitte einen korrekten Wert für den Benutzernamen ein.<br />";
            }
        } else {
            $error .= "Geben Sie bitte Ihren Benutzernamen ein.<br />";
        }


        if (empty($error)) {
            if ($username !== $_SESSION['username']) {
                $error .= "Der eingegebene Wert stimmt nicht mit dem Benutzernamen überein!";
            } else {
                $id = $_SESSION['id'];
                $query = "DELETE FROM user WHERE id=?";
                $stmt = $mysqli->prepare($query);
                if ($stmt === false) {
                    $error .= 'prepare() failed ' . $mysqli->error . '<br />';
                }
                if (!$stmt->bind_param('i', $id)) {
                    $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
                }
                if (!$stmt->execute()) {
                    $error .= 'execute() failed ' . $mysqli->error . '<br />';
                }

                $result = $stmt->get_result();
                $stmt->close();

                header('Location: index.php?page=logout');
            }
        }
    }
} else {
    $error .= 'Sie sind nicht eingeloggt!';
}
?>
<div class="container">
    <div class="card">
        <div class="card-content">
            <form action="?page=delete-user" method="post">
                <div class="is-size-2">Account löschen</div>

                <?php
                if (!empty($error)) {
                    echo "<div class=\"notification is-danger\" role=\"alert\">" . $error . "</div>";
                }
                ?>

                <p class="is-size-5 mb-2">Sind Sie sich sicher das Sie Ihren Benutzer löschen wollen? Diese Aktion kann nicht rückgängig gemacht werden!</p>
                <p class="is-size-5 mb-4">Bitte geben Sie folgend Ihren Benutzernamen <strong><?php echo $_SESSION['username'] ?></strong> ein um die Aktion zu bestätigen.</p>

                <div class="field">
                    <label class="label">Benutzername</label>
                    <div class="control has-icons-left">
                        <input type="text" name="username" class="input" id="username" value="<?php echo $username ?>" placeholder="Gross- und Keinbuchstaben, min. 6 Zeichen, max. 30 Zeichen" pattern="(?=.*[a-z])(?=.*[A-Z])[a-zA-Z]{6,}" title="Gross- und Keinbuchstaben, min 6 Zeichen, max 30 Zeichen" maxlength="30" required="true" />
                        <span class="icon is-small is-left">
                            <i class="fas fa-user"></i>
                        </span>
                    </div>
                </div>

                <input type="hidden" name="submitted" value="true" />

                <div>
                    <button type="submit" name="button" value="submit" class="button is-danger">Accout löschen</button>
                    <a class="button is-light" href="?page=profile&mode=read">
                        Abbrechen
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>