<?php
$username = $_SESSION['username'];
$error = $message = $stored_filename = '';
$picture_exists = false;
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true) {
    if ($_SERVER['REQUEST_METHOD'] == "POST" && !isset($_REQUEST['link'])) {

        if (isset($_POST['firstname'])) {
            $firstname = htmlentities(strip_tags(trim($_POST['firstname'])));

            if (empty($firstname) || strlen($firstname) > 45) {
                $error .= "Geben Sie bitte einen korrekten Vornamen ein.<br />";
            }
        } else {
            $error .= "Geben Sie bitte einen Vornamen ein.<br />";
        }

        if (isset($_POST['lastname'])) {
            $lastname = htmlentities(strip_tags(trim($_POST['lastname'])));

            if (empty($lastname) || strlen($lastname) > 45) {
                $error .= "Geben Sie bitte einen korrekten Nachnamen ein.<br />";
            }
        } else {
            $error .= "Geben Sie bitte einen Nachnamen ein.<br />";
        }

        if (isset($_POST['username'])) {
            $username = htmlentities(strip_tags(trim($_POST['username'])));

            if (empty($username) || !preg_match("/(?=.*[a-z])(?=.*[A-Z])[a-zA-Z]{6,30}/", $username)) {
                $error .= "Geben Sie bitte einen korrekten Usernamen ein.<br />";
            }
        } else {
            $error .= "Geben Sie bitte einen Username ein.<br />";
        }

        if (isset($_POST['email'])) {
            $email = htmlentities(strip_tags(trim($_POST['email'])));

            if (strlen($email) > 60 || !empty($email) && !preg_match("/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4}/", $email)) {
                $error .= "Geben Sie bitte eine korrekte E-Mail ein.<br />";
            }
        } else {
            $error .= "Geben Sie bitte eine E-Mail ein.<br />";
        }

        validate_and_create_attachment();

        if (empty($error)) {
            if ($username !== $_SESSION['username']) {
                $query = "SELECT * FROM user where username=? ";
                $stmt = $mysqli->prepare($query);

                if ($stmt === false) {
                    $error .= 'prepare() failed ' . $mysqli->error . '<br />';
                }
                if (!$stmt->bind_param('s', $username)) {
                    $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
                }
                if (!$stmt->execute()) {
                    $error .= 'execute() failed ' . $mysqli->error . '<br />';
                }
                $result = $stmt->get_result();
                $stmt->close();
                if ($result->num_rows !== 0) {
                    $error .= 'Benutzername existiert schon, bitte wählen Sie einen anderen!<br />';
                }
            }
            if (empty($error)) {
                $mysqli->begin_transaction();
                if ($stored_filename !== '') {
                    $attachment_id = create_attachment($stored_filename, $_FILES["imageUpload"]["name"]);
                    $query = "UPDATE user set username=?, prename=?, lastname=?, email=?, picture_id=? WHERE id=?";
                    $stmt = $mysqli->prepare($query);

                    if ($stmt === false) {
                        $error .= 'prepare() failed ' . $mysqli->error . '<br />';
                    }
                    if (!$stmt->bind_param('ssssii', $username, $firstname, $lastname, $email, $attachment_id, $_SESSION['id'])) {
                        $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
                    }
                } else {
                    $query = "UPDATE user set username=?, prename=?, lastname=?, email=? WHERE id=?";
                    $stmt = $mysqli->prepare($query);

                    if ($stmt === false) {
                        $error .= 'prepare() failed ' . $mysqli->error . '<br />';
                    }
                    if (!$stmt->bind_param('ssssi', $username, $firstname, $lastname, $email, $_SESSION['id'])) {
                        $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
                    }
                }
                if (!$stmt->execute()) {
                    $error .= 'execute() failed ' . $mysqli->error . '<br />';
                }
                if (isset($_REQUEST['old_picture_id'])) {
                    delete_attachment($_REQUEST['old_picture_id']);
                }
                $mysqli->commit();
                $_SESSION['username'] = $username;
                $message .= "Änderungen wurden erfolgreich gespeichert!<br />";
            }
        }
    } else {
        $query = "SELECT * FROM user WHERE username=?";
        $stmt = $mysqli->prepare($query);

        if ($stmt === false) {
            $error .= 'prepare() failed ' . $mysqli->error . '<br />';
        }
        // Daten an das SQL-Statement binden
        if (!$stmt->bind_param('s', $username)) {
            $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
        }
        if (!$stmt->execute()) {
            $error .= 'execute() failed ' . $mysqli->error . '<br />';
        }

        $result = $stmt->get_result();
        $stmt->close();

        if ($result->num_rows === 0) {
            $user_not_found = true;
        } else {
            $result = $result->fetch_assoc();
            $firstname = $result['prename'];
            $lastname = $result['lastname'];
            $email = $result['email'];
            if ($result['picture_id']) {
                $picture_exists = true;
            }
        }
    }
} else {
    $error .= 'Sie sind nicht eingeloggt!';
}
?>
<div class="container">
    <div class="card">
        <div class="card-content">
            <div class="is-size-2">Benutzerinformationen anpassen</div>
            <?php
            if (!empty($error)) {
                echo "<div class=\"notification is-danger\" role=\"alert\">" . $error . "</div>";
            } else if (!empty($message)) {
                echo "<div class=\"notification is-success\" role=\"alert\">" . $message . "</div>";
            }
            ?>
            <form action="?page=profile&mode=edit" method="post" enctype="multipart/form-data">
                <div class="field">
                    <label class="label" for="firstname">Vorname *</label>
                    <div class="control has-icons-left">
                        <input type="text" name="firstname" class="input" id="firstname" value="<?php echo $firstname ?>" placeholder="Geben Sie Ihren Vornamen an." maxlength="45" required="true">
                        <span class="icon is-small is-left">
                            <i class="fas fa-user"></i>
                        </span>
                    </div>
                </div>

                <div class="field">
                    <label class="label" for="lastname">Nachname *</label>
                    <div class="control has-icons-left">
                        <input type="text" name="lastname" class="input" id="lastname" value="<?php echo $lastname ?>" placeholder="Geben Sie Ihren Nachnamen an" maxlength="45" required="true">
                        <span class="icon is-small is-left">
                            <i class="fas fa-user"></i>
                        </span>
                    </div>
                </div>

                <div class="field">
                    <label class="label" for="email">Email</label>
                    <div class="control has-icons-left">
                        <input type="email" name="email" class="input" id="email" pattern="[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4}" value="<?php echo $email ?>" placeholder="Geben Sie Ihre E-Mail an" title="Geben Sie eine korrekte E-Mail an" maxlength="60">
                        <span class="icon is-small is-left">
                            <i class="fas fa-envelope"></i>
                        </span>
                    </div>
                </div>

                <div class="field">
                    <label class="label" for="username">Benutzername *</label>
                    <div class="control has-icons-left">
                        <input type="text" name="username" class="input" id="username" value="<?php echo $username ?>" placeholder="Gross- und Keinbuchstaben, min 6 Zeichen, max. 30 Zeichen" pattern="(?=.*[a-z])(?=.*[A-Z])[a-zA-Z]{6,}" title="Gross- und Keinbuchstaben, min 6 Zeichen, max. 30 Zeichen" maxlength="30" required="true">
                        <span class="icon is-small is-left">
                            <i class="fas fa-user"></i>
                        </span>
                    </div>
                </div>

                <div class="field">
                    <?php if ($picture_exists) : ?>
                        <label class="label" for="exampleFormControlFile1">Neues Profilbild</label>
                    <?php else : ?>
                        <label class="label" for="exampleFormControlFile1">Profilbild</label>
                    <?php endif ?>
                    <div class="file is-boxed">
                        <label class="file-label">
                            <input class="file-input" type="file" name="imageUpload" id="imageUpload" accept=".jpg,.png,.jpeg" />
                            <span class="file-cta">
                                <span class="file-icon">
                                    <i class="fas fa-upload"></i>
                                </span>
                                <span class="file-label">
                                    Laden Sie hier das Bild hoch…
                                </span>
                            </span>
                        </label>
                    </div>
                    <div class="is-size-7">Nur .JPG, .PNG und .JPEG Formate werden akzeptiert.</div>
                </div>

                <input type="hidden" name="submitted" value="true" />
                <?php if ($picture_exists) : ?>
                    <input type="hidden" name="old_picture_id" value="<?php echo $result['picture_id'] ?>" />
                <?php endif ?>

                <button type="submit" name="button" value="submit" class="button is-primary">Speichern</button>
                <a class="button is-light" href="?page=profile&mode=read">
                    Zurück
                </a>
            </form>
        </div>
    </div>
</div>