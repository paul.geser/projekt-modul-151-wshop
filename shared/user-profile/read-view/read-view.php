<?php
$user_not_found = false;
$username = $_SESSION['username'];
$id = $_SESSION['id'];
$error = '';
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true) {
    $query = 'SELECT u.*, a.filename, a.old_filename
            FROM user u LEFT JOIN attachment a
            ON u.picture_id = a.id
            where u.id=?';
    $stmt = $mysqli->prepare($query);
    if ($stmt === false) {
        $error .= 'prepare() failed ' . $mysqli->error . '<br />';
    }
    if (!$stmt->bind_param('i', $id)) {
        $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
    }
    if (!$stmt->execute()) {
        $error .= 'execute() failed ' . $mysqli->error . '<br />';
    }

    $result = $stmt->get_result();
    $stmt->close();

    if ($result->num_rows === 0) {
        $user_not_found = true;
    } else {
        $result = $result->fetch_assoc();
    }
} else {
    $error .= 'Sie sind nicht eingeloggt!';
}
?>
<div class="container">
    <div class="card">
        <div class="card-content">
            <?php
            if (!empty($error)) {
                echo "<div class=\"notification is-danger\" role=\"alert\">" . $error . "</div>";
            }
            ?>
            <div class="is-size-2">Benutzerinformationen</div>
            <div class="is-flex is-flex-direction-row">
                <div class="profile-read-component-picture-outside-box">
                    <div class="profile-read-component-picture-box">
                        <?php if ($result['picture_id']) : ?>
                            <img src="./assets/uploads/<?php echo $result['filename'] ?>" alt="<?php echo $result['old_filename'] ?>" class="profile-read-component-picture">
                        <?php else : ?>
                            <img src="./assets/placeholder.jpg" alt="item-picture" class="profile-read-component-picture">
                        <?php endif ?>
                    </div>
                </div>
                <div class="is-flex is-flex-direction-column ml-6">
                    <div class="mb-4 mt-2">
                        <div class="is-size-6">Benutzername</div>
                        <div class="is-size-5 mb-2"><strong><?php echo $result['username'] ?></strong></div>
                        <div class="is-size-6">Vorname</div>
                        <div class="is-size-5 mb-2"><strong><?php echo $result['prename'] ?></strong></div>
                        <div class="is-size-6">Nachname</div>
                        <div class="is-size-5 mb-2"><strong><?php echo $result['lastname'] ?></strong></div>
                        <div class="is-size-6">Email</div>
                        <?php if ($result['email'] !== '') : ?>
                            <div class="is-size-5 mb-2"><strong><?php echo $result['email'] ?></strong></div>
                        <?php else : ?>
                            <div class="is-size-5 mb-2"><strong>-</strong></div>
                        <?php endif ?>
                    </div>
                    <div class="buttons">
                        <a class="button is-primary" href="?page=profile&mode=edit">
                            Profil bearbeiten
                        </a>
                        <a class="button is-info" href="?page=profile&mode=password">
                            Passwort ändern
                        </a>
                        <a class="button is-danger" href="?page=delete-user">
                            Benutzer löschen
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>