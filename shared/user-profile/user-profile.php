<?php

if (isset($_REQUEST['mode'])) {
    $mode = $_REQUEST['mode'];
} else {
    $mode = 'read';
}


switch ($mode) {
    case 'read':
        include('shared/user-profile/read-view/read-view.php');
        break;
    case 'edit':
        include('shared/user-profile/edit-view/edit-view.php');
        break;
    case 'password':
        include('shared/user-profile/password-change/password-change.php');
        break;
    default:
        header('Location: index.php?page=home');
        break;
}
