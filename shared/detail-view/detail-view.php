<?php
$url_invalid = false;
$item_not_found = false;
?>
<div class="container">
    <div class="card">
        <div class="card-content">
            <?php
            if (!empty($error)) {
                echo "<div class=\"notification is-danger\" role=\"alert\">" . $error . "</div>";
            }
            ?>
            <?php
            if (isset($_REQUEST['item-id'])) {
                $item_id = $_REQUEST['item-id'];
                $query = 'SELECT p.*, a.filename, a.old_filename, u.username, ua.filename AS owner_filename
                    FROM product p 
                    LEFT JOIN attachment a ON p.picture_id = a.id
                    LEFT JOIN user u ON p.owner = u.id
                    LEFT JOIN attachment ua ON u.picture_id = ua.id
                    where p.id =?';
                $stmt = $mysqli->prepare($query);
                if ($stmt === false) {
                    $error .= 'prepare() failed ' . $mysqli->error . '<br />';
                }
                if (!$stmt->bind_param('i', $item_id)) {
                    $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
                }
                if (!$stmt->execute()) {
                    $error .= 'execute() failed ' . $mysqli->error . '<br />';
                }
                $result = $stmt->get_result();
                $stmt->close();
                if ($result->num_rows === 0) {
                    $item_not_found = true;
                } else {
                    $item_not_found = false;
                    $result = $result->fetch_assoc();
                }
            } else {
                $url_invalid = true;
            }
            ?>
            <div class="is-size-2">Detailansicht</div>
            <?php if ($url_invalid == true) : ?>
                <div class="is-size-3">URL is invalid</div>
            <?php elseif ($item_not_found == true) : ?>
                <div class="is-size-3">Produkt konnte nicht gefunden werden</div>
            <?php else : ?>
                <?php if (isset($result['new_owner']) && isset($_SESSION['id']) && $result['new_owner'] === $_SESSION['id']) : ?>
                    <div class="notification is-info is-light">Sie besitzen dieses Produkt!</div>
                <?php endif ?>
                <div class="item-detailview-component-top-main-content-box">
                    <div class="item-detailview-component-top-main-image-box">
                        <?php if ($result['picture_id']) : ?>
                            <img class="item-detailview-component-top-main-image" src="./assets/uploads/<?php echo $result['filename'] ?>" alt="<?php echo $result['old_filename'] ?>" width="700">
                        <?php else : ?>
                            <img class="item-detailview-component-top-main-image" src="./assets/placeholder.jpg" alt="item-picture" height="500">
                        <?php endif ?>
                    </div>
                    <div class="item-detailview-component-top-main-sub-content-box">
                    <?php if (is_decimal($result['price'])) : ?>
                        <div class="has-text-danger is-size-3"><strong class="has-text-danger is-size-3"><?php echo $result['price'] ?></strong></div>
                        <?php else : ?>
                            <div class="has-text-danger is-size-3"><strong class="has-text-danger is-size-3"><?php echo $result['price'] ?>.-</strong></div>
                            <?php endif ?>
                        <div class="is-size-2"><strong><?php echo $result['name'] ?></strong></div>
                        <div class="has-text-grey"><?php echo $result['creation_date'] ?></div>
                        <div class="buttons">
                            <?php if (isset($_SESSION['id']) && $result['owner'] === $_SESSION['id'] && $result['bought'] !== 1) : ?>
                                <a class="button is-info" href="?page=edit-item&id=<?php echo $result['id'] ?>">
                                    <strong>Bearbeiten</strong>
                                </a>
                                <a class="button is-danger" href="?page=delete-item&id=<?php echo $result['id'] ?>&picture-id=<?php echo $result['picture_id'] ?>">
                                    <strong>Löschen</strong>
                                </a>
                            <?php elseif ($result['bought'] === 0) : ?>
                                <a class="button is-primary" href="?page=buy&item-id=<?php echo $result['id'] ?>">
                                    <strong>Kaufen</strong>
                                </a>
                            <?php endif ?>
                            <a class="button is-light" href="?page=list">
                                <strong>Zurück</strong>
                            </a>
                        </div>
                        <div class="is-flex is-flex-direction-row is-align-items-center">
                            <div class="item-detailview-component-picture-box">
                                <?php if ($result['owner_filename']) : ?>
                                    <img src="./assets/uploads/<?php echo $result['owner_filename'] ?>" alt="owner_picture" class="item-detailview-component-picture">
                                <?php else : ?>
                                    <img src="./assets/placeholder.jpg" alt="item-picture" class="item-detailview-component-picture">
                                <?php endif ?>
                            </div>
                            <div class="ml-3"><?php echo $result['username'] ?></div>
                        </div>
                    </div>
                </div>
                <div class="item-detailview-component-bottom-main-content-box mt-5">
                    <article class="message is-primary">
                        <div class="message-header">
                            <p>Kurz Beschreibung</p>
                        </div>
                        <div class="message-body">
                            <?php echo $result['short_description'] ?>
                        </div>
                    </article>
                    <?php if ($result['long_description'] !== '') : ?>
                        <article class="message is-primary">
                            <div class="message-header">
                                <p>Lange Beschreibung</p>
                            </div>
                            <div class="message-body">
                                <?php echo $result['long_description'] ?>
                            </div>
                        </article>
                    <?php endif ?>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>