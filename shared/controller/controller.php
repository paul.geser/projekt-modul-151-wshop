<?php

if (isset($_REQUEST['page'])) {
    $page = $_REQUEST['page'];
} else {
    $page = 'list';
}

switch ($page) {
    case 'login':
        include('shared/login/login.php');
        break;
    case 'logout':
        $_SESSION = array();
        session_destroy();
        header('Location: index.php?page=home');
        exit();
        break;
    case 'register':
        include('shared/register/register.php');
        break;
    case 'list':
        include('shared/list/list.php');
        break;
    case 'detail-view':
        include('shared/detail-view/detail-view.php');
        break;
    case 'edit-item':
        include('shared/edit-item/edit-item.php');
        break;
    case 'delete-item':
        include('shared/delete-item/delete-item.php');
        break;
    case 'profile':
        include('shared/user-profile/user-profile.php');
        break;
    case 'delete-user';
        include('shared/user-profile/delete-view/delete-view.php');
        break;
    case 'buy':
        include('shared/buy-view/buy-view.php');
        break;
    case 'bought-items';
        include('shared/bought-items/bought-items.php');
        break;
    case 'home':
        include('shared/list/list.php');
        break;
    default:
        include('shared/list/list.php');
        break;
}
