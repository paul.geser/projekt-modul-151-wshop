<?php
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true) {
    if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_REQUEST['id']) && isset($_POST['submitted']) && $_POST['submitted'] == true) {
        $query = "DELETE FROM product WHERE id=?";
        $stmt = $mysqli->prepare($query);

        if ($stmt === false) {
            $error .= 'prepare() failed ' . $mysqli->error . '<br />';
        }
        if (!$stmt->bind_param('i', $_REQUEST['id'])) {
            $error .= 'bind_param() failed ' . $mysqli->error . '<br />';
        }
        if (!$stmt->execute()) {
            $error .= 'execute() failed ' . $mysqli->error . '<br />';
        }
        $result = $stmt->get_result();

        if (isset($_REQUEST['picture-id']) && $_REQUEST['picture-id'] !== '') {
            delete_attachment($_REQUEST['picture-id']);
        }

        $stmt->close();
        if (empty($error)) {
            header('Location: index.php?page=list');
            exit();
        }
    }
} else {
    $error .= 'Sie sind nicht eingeloggt!';
}
?>
<div class="container">
    <div class="card">
        <div class="card-content">
            <?php
            if (!empty($error)) {
                echo "<div class=\"notification is-danger\" role=\"alert\">" . $error . "</div>";
            }
            ?>
            <div class="is-size-2">Produkt löschen</div>
            <div class="is-size-4 mt-1 mb-2"><strong>Sind Sie sich sicher das Sie das Produkt löschen wollen?</strong></div>
            <div class="notification is-info is-light">Eine Wiederherstellung nach dem Löschen des Produktes ist nicht mehr möglicht!</div>
            <form action="?page=delete-item&id=<?php echo $_REQUEST['id'] ?>&picture-id=<?php echo $_REQUEST['picture-id'] ?>" method="post">
                <input type="hidden" name="submitted" value="true" />
                <button type="submit" name="button" value="submit" class="button is-danger"><strong>Bestätigen</strong></button>
                <button type="submit" name="button" value="submit" class="button is-light" formaction="?page=detail-view&item-id=<?php echo $_REQUEST['id'] ?>"><strong>Abbrechen</strong></button>
            </form>
        </div>
    </div>
</div>